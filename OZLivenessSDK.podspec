Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '8.15.0'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://gitlab.com/oz-forensics/oz-liveness-ios'
  s.authors = { 'oz-forensics' => 'info@ozforensics.com' }
  s.source = { :git => 'https://gitlab.com/oz-forensics/oz-liveness-ios', tag: "#{s.version}" }
  s.ios.deployment_target  = '11.0'
  s.default_subspec = 'Full'
  
  s.swift_versions = ['4.2', '5.0']
  
  s.license = { type: 'Commercial', text: '© 2023 OZForensics. All rights reserved.\n' }
  
  s.prepare_command = <<-CMD
    VERSION=#{s.version}
    curl -L -o OZLivenessSDK.xcframework.zip https://gitlab.com/oz-forensics/oz-liveness-ios/raw/${VERSION}/OZLivenessSDK.xcframework.zip
    unzip OZLivenessSDK.xcframework.zip
    rm OZLivenessSDK.xcframework.zip
  CMD
  
  s.static_framework = true
  s.pod_target_xcconfig = { 'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES' }
  
  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = ['OZLivenessSDKResources.bundle', 'OZLivenessSDKOnDeviceResources.bundle']
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = 'OZLivenessSDKResources.bundle'
  end
  
  s.dependency 'TensorFlowLiteC', '2.11.0'

end

